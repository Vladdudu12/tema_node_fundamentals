let rezervari =
{
    numarRezervariTotale : 3,
    numePersoane : ["Croco","Tania","Adrian"],
    nrPersoaneLaMasa : [6,2,3]
}

console.log(rezervari);
rezervari.numarRezervariTotale++;
rezervari.numePersoane.push("Mihnea");
rezervari.nrPersoaneLaMasa.splice(3,0,10);


console.log(rezervari);

let functie = () =>
{
    let sum = 0;
    let vector = [];
    if(rezervari.numePersoane.length === rezervari.nrPersoaneLaMasa.length)
    {
        for(let i = 0; i < rezervari.numePersoane.length; i++)
        {
            sum += rezervari.nrPersoaneLaMasa[i];
            vector.push(rezervari.numePersoane[i] + "-" + rezervari.nrPersoaneLaMasa[i])
        }
        console.log(vector);
        return sum;
    }
    else
    {
        console.log("Numarul de rezervari nu coincide")
    }
}

functie();