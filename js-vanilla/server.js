//afisarea la consola
console.log("Hello crocos");

let var1 = 3; //let --> small scale
var1 = 5;
console.log(var1);

const var2 = 6;
//var2 = 9; YOU CANNOT CHANGE THE CONST, YOU IDIOT
console.log(var2);

var var3 = "crocos"; //var --> large scale
console.log(var3);

function greeting()
{
    console.log("Hello world!");
}

function greeting(name)
{
    console.log("Hello " + name);
}


function greeting(name, age)
{
    console.log("Hello " + name + " cu varsta de: " + age + " ani");
}
greeting();
// greeting("Vlad");
// greeting("Vlad","19");

let sum = function (a,b)
{
    return a+b;
}
console.log(sum(2,3));

let x = sum(4,4);
console.log(x);


let arrowFunction = (n) => 
{
    return n*(n+1)/2
}

console.log(arrowFunction(7));

let array = [12, 3, "banana", 9.0];

console.log(array[2]);

array.push(10);
console.log(array);
array.pop();
console.log(array);
array.splice(2,1);
console.log(array);
array.splice(2,0,"ana");
console.log(array);


let propozitie = "Imi place sa codez";
let cuvinte = propozitie.split(" ");
console.log(cuvinte);
let fructe = "banana,mar,para";
let fructeVector = fructe.split(",");
console.log(fructeVector);


if(1 == 1) 
{
    console.log("egale");
}else
{
    console.log("nu sunt egale");
}


for(let i = 0; i < fructeVector.length; i++)
{
    console.log(fructeVector[i]);
}

let obiect = {
    nume : "Gigel",
    varsta : 20,
    adresa : ["Bucuresti","Pitesti"],
    data : {
        zi : 25,
        luna : 11,
        an: 2021,
    },
}
obiect.nume = "Croco";
obiect.data.luna = 12;
console.log(obiect);
